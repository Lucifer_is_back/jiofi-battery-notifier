﻿/*
 * Created by SharpDevelop.
 * User: Lucifer
 * Date: 12/01/2019
 * Time: 12:26 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace jiofi_battery_notifier
{
    public sealed class NotificationIcon
    {
        private NotifyIcon notifyIcon;
        private ContextMenu notificationMenu;
        private Thread thread;
        private bool running;

        #region Initialize icon and menu
        public NotificationIcon()
        {
            running = true;
            notifyIcon = new NotifyIcon();
            notificationMenu = new ContextMenu(InitializeMenu());

            notifyIcon.DoubleClick += IconDoubleClick;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NotificationIcon));
            notifyIcon.Icon = (Icon)resources.GetObject("$this.Icon");
            notifyIcon.ContextMenu = notificationMenu;
        }

        private MenuItem[] InitializeMenu()
        {
            MenuItem[] menu = new MenuItem[] {
                new MenuItem("Get", menuAboutClick),
                new MenuItem("Exit", menuExitClick)
            };
            return menu;
        }
        #endregion

        #region Main - Program entry point
        /// <summary>Program entry point.</summary>
        /// <param name="args">Command Line Arguments</param>
        [STAThread]
        public static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            bool isFirstInstance;
            // Please use a unique name for the mutex to prevent conflicts with other programs
            using (Mutex mtx = new Mutex(true, "jiofi_battery_notifier", out isFirstInstance))
            {
                if (isFirstInstance)
                {
                    NotificationIcon notificationIcon = new NotificationIcon();
                    notificationIcon.notifyIcon.Text = "Battery %";
                    notificationIcon.notifyIcon.Visible = true;
                    Application.Run();
                    notificationIcon.notifyIcon.Dispose();
                }
                else
                {
                    // The application is already running
                    // TODO: Display message box or change focus to existing application instance
                }
            } // releases the Mutex
        }
        #endregion

        #region Event Handlers
        private void menuAboutClick(object sender, EventArgs e)
        {
            thread = new Thread(processThread);
            thread.Start();
        }

        private void processThread()
        {
            while (running)
            {

                try
                {
                    String URLString = "http://jiofi.local.html/st_dev.w.xml";
                    XmlTextReader tr = new XmlTextReader(URLString);
                    //TextReader tr = new StringReader("<dev><batt_per>24</batt_per><batt_st>512</batt_st></dev>");
                    XDocument doc = XDocument.Load(tr);

                    var query = doc.Descendants("dev")
                   .Select(element => new
                   {
                       Level = (int)element.Element("batt_per"),
                       Status = (int)element.Element("batt_st")
                   })
                   .Single();
                    string msg = "";
                    if (query.Level < 6 && query.Status < 1024)
                    {
                        msg = "Connect USB Charger Immediately!";
                    }
                    else
                    if (query.Level < 6 && query.Status < 1024)
                    {
                        msg = "Device Fully Charged!";
                    }
                    else
                    {
                        msg = "Device Battery State Normal!";
                    }
                    notifyIcon.ShowBalloonTip(5000, "JioFi Battery Notifier\n", msg, ToolTipIcon.Info);
                }
                catch (Exception ex)
                {

                    ;
                }
                Thread.Sleep(5000);
            }
        }

        private void menuExitClick(object sender, EventArgs e)
        {
            running = false;
            thread?.Join();
            Application.Exit();
        }

        private void IconDoubleClick(object sender, EventArgs e)
        {
            MessageBox.Show("The icon was double clicked");
        }
        #endregion
    }
}
