# JioFi Battery Notifier

Desktop notifications for battery warnings (low and charged) alerts of [JioFi](https://www.jio.com/shop/en-in/c/jiofi) device

## Installation

#### Installer script

For any *nix machine with `systemd` init system:

`curl -sSL https://gitlab.com/icp1994/jiofi-battery-notifier/raw/master/installer | bash`

#### Build from source

You need to addtionally have `git` and the [Go](https://golang.org/) compiler.

    git clone https://gitlab.com/icp1994/jiofi-battery-notifier.git
    cd jiofi-battery-notifier
    mkdir -p $HOME/.jiofi-battery-notifier ; cp logo.svg $_
    mkdir -p $HOME/.local/bin ; go build -o $_/jiofi-battery-notifier
    mkdir -p $HOME/.local/share/systemd/user ; cp jiofi-battery-notifier.service jiofi-battery-notifier.timer $_
    systemctl --user daemon-reload
    systemctl --user --now enable jiofi-battery-notifier.timer