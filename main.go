package main

import (
	"encoding/xml"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strconv"
)

func main() {
	type Battery struct {
		Level  string `xml:"batt_per"`
		Status string `xml:"batt_st"`
	}

	resp, err := http.Get("http://jiofi.local.html/st_dev.w.xml")
	if err != nil {
		log.Fatal(err)
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		log.Fatal(err)
	}

	battery := Battery{Level: "none", Status: "none"}
	err = xml.Unmarshal(body, &battery)
	if err != nil {
		log.Fatal(err)
	}

	level, err := strconv.ParseInt(battery.Level, 10, 16)
	if err != nil {
		log.Fatal(err)
	}
	status, err := strconv.ParseInt(battery.Status, 10, 16)
	if err != nil {
		log.Fatal(err)
	}
	msg := ""
	lvl := "normal"

	if level < 6 && status < 1024 {
		msg = "Connect USB Charger Immediately!"
		lvl = "critical"
	}

	if level == 100 && status > 1280 {
		msg = "Device Fully Charged!"
	}

	if msg != "" {
		cmd := exec.Command(
			"notify-send", "JioFi Battery Notifier\n", msg,
			"--urgency", lvl,
			"--expire-time", "20000",
			"--icon", os.Getenv("HOME")+"/.jiofi-battery-notifier/logo.svg",
		)

		err = cmd.Run()
		if err != nil {
			log.Fatal(err)
		}
	}
}
